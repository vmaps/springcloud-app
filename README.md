# Spring Cloud 微服务架构

本项目是一个基于 Spring Cloud 的微服务架构示例，包含多个服务模块，使用 Consul 进行服务发现和配置管理。

## 项目结构

```
springcloud-app
├── config                  # 配置服务
├── gateway                 # 网关服务
├── hystrix-dashboard       # Hystrix 仪表盘
├── order                   # 订单服务
├── turbine                 # Turbine 服务
└── user                    # 用户服务
```

## 技术栈

- **Spring Boot**: 2.2.5.RELEASE
- **Spring Cloud**: Hoxton.SR3
- **Consul**: 服务发现和配置管理
- **Hystrix**: 断路器模式
- **Feign**: 声明式 HTTP 客户端
- **RabbitMQ**: 消息队列

## 启动服务

1. 启动 Consul 服务：
   ```bash
   consul agent -dev
   ```

2. 启动配置服务：
   ```bash
   cd config
   mvn spring-boot:run
   ```

3. 启动网关服务：
   ```bash
   cd gateway
   mvn spring-boot:run
   ```

4. 启动用户服务：
   ```bash
   cd user
   mvn spring-boot:run
   ```

5. 启动订单服务：
   ```bash
   cd order
   mvn spring-boot:run
   ```

6. 启动 Hystrix 仪表盘：
   ```bash
   cd hystrix-dashboard
   mvn spring-boot:run
   ```

7. 启动 Turbine 服务：
   ```bash
   cd turbine
   mvn spring-boot:run
   ```

## API 接口

### 用户服务

- **获取用户信息**
  - **URL**: `/user/getUser`
  - **方法**: POST
  - **参数**: `user` (String)

### 订单服务

- **获取订单信息**
  - **URL**: `/order/get`
  - **方法**: GET
  - **参数**: `text` (String)

## 配置文件

各个服务的配置文件位于 `src/main/resources/bootstrap.properties` 和 `src/main/resources/application.properties` 中。

## 许可证

本项目采用 Apache License 2.0 许可证，详细信息请查看 [LICENSE](LICENSE) 文件。
